## Ecommerce Application - Laravel

Basic Ecommerce Application using Laravel and React

---

## About Application Project

**Technology Used:**

1. Laravel 7.0.3
1. PHP 7.4
1. React JS
1. Visual Studio Code

---

## Application Features `Frontend Panel`

> **User Authentication**

1. Registration
1. Login
1. Forget Password
1. Reset Password

> **Products**

1. List Products
1. Details Product
   1. Product Image Zooming
   1. Links - Add To Cart, Buy, Wishlist
1. Details Product - Product Attribute Details
1. Details Product - Product Review System
1. Paginated Products
1. Trending Products
1. Offer/Discount Products (10%, 20%)

> **Product Searching**

1. Product Search by Keywords
1. Product By Category
1. Product Filter
1. Filter By -
   1. Price
   1. Category
   1. Brand
   1. Attribute

> **Coupon System**

1. Check Coupon By Coupon Code

> **Cart**

1. List Cart
1. Add to Cart
1. Edit Cart
1. Delete Cart

> **Wishlist**

1. List Wishlist
1. Add to Wishlist
1. Delete Wishlist

> **Checkout**

1. Payment Gateway Integration
   1. Paypal
   1. Stripe
   1. Visa/MasterCard
   1. Neteller
1. Create Checkout

> **User**

1. User Profile Information
1. User Profile Edit
1. User Profile Picture Upload
1. User Shipping Address Update
1. User Billing Address Update

> **Product Compare System**

1. Compare products (`Samsung S20s vs Samsung S20 Pro`)

---

## Application Features `Admin Panel`

> **Admin Authentication**

1. Registration
1. Login
1. Forget Password
1. Reset Password

> **Category Management**

1. List, Create, Edit, Delete

> **Brand Management**

1. List, Create, Edit, Delete

> **Product Management**

1. List, Create, Edit, Delete
1. Product Price Management
1. Product Stock Management
1. Product Attribute Management

> **Order Management**

1. List, Create, Edit, Delete
1. Invoice Generate

> **Coupon Management**

1. List, Create, Edit, Delete

> **Advertising Management**

1. List, Create, Edit, Delete

> **User Management**

1. List, Create, Edit, Delete
1. Rule Management
1. Permission Management

> **Website Settings**

1. Website Information System Management
1. Slider Management
