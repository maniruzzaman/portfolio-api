## Ecommerce API - Laravel, Passport

Basic API Implementation of an Ecommerce using Laravel and Passport

## About API Project

**Requirements:**

1. PHP > 7.2.5

## API Features

> **Authentication**

1. Registration `post`
1. Login `post`
1. Forget Password `post`
1. Reset Password `post`

> **Products**

1. List Products `get`
1. Create Product `post`
1. Edit Product `put`
1. Delete Product `delete`
1. Paginated Products `get`

> **Product Searching**

1. Product Search by Keywords `get`
1. Product By Category `get`
1. Product Filter `get`
1. Filter By - `get`
   1. Price `get`
   1. Category `get`
   1. Brand `get`
   1. Attribute `get`

> **Coupon System**

1. List Coupons `get`
1. Create Coupon `post`
1. Edit Coupon `put`
1. Delete Coupon `delete`
1. Check Coupon By Coupon Code `get`

> **Cart**

1. List Cart `get`
1. Add to Cart `post`
1. Edit Cart `put`
1. Delete Cart `delete`

> **Checkout**

1. Create Checkout `post`

> **User**

1. User Profile Information `get`
1. User Profile Edit `put`
1. User Profile Picture Upload `post`
1. User Shipping Address Update `get`
1. User Billing Address Update `get`
